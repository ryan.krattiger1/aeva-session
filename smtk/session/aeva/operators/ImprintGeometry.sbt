<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "imprint geometry" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="Resample geometry onto an image" Label="Imprint Geometry" BaseType="operation">
      <BriefDescription>Resample geometry onto an image.</BriefDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="2" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face|volume"/></Accepts>
      </AssociationsDef>
	  <ItemDefinitions>
	    <Double Name="bandwidth" Label="Bandwidth" Units="??" Optional="false" IsEnabledByDefault="true">
          <BriefDescription>The distance to splat the surface with.</BriefDescription>
          <DefaultValue>2.0</DefaultValue>
	    </Double>
	  </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(interpolated geometry)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
