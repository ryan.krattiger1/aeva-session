//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_BooleanUnite_h
#define smtk_session_aeva_BooleanUnite_h

#include "smtk/session/aeva/operators/Boolean.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/// Unite side sets by global ID.
class SMTKAEVASESSION_EXPORT BooleanUnite : public Boolean
{
public:
  smtkTypeMacro(smtk::session::aeva::BooleanUnite);
  smtkCreateMacro(BooleanUnite);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Boolean);

  Boolean::BooleanOpType type() const override { return Boolean::Unite; }

protected:
  virtual const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_BooleanUnite_h
