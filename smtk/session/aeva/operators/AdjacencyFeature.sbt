<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "adjacency feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="adjacency feature" Label="Adjacency Feature" BaseType="operation">

      <BriefDescription>Generate a surface selection by choosing cells adjacent to a set of other surfaces.</BriefDescription>
      <DetailedDescription>
        Generate a surface selection by choosing cells adjacent to a set of other surfaces.
        Adjacency is defined via the angle between a cell's normal vector and a vector
        from the cell center to the center of the target object's bounding box.
        When the angle is smaller than the provided angle tolerance, the cell is considered adjacent.
        An option is provided for restricting the set of cells further by requiring distance
        between adjacent primitives to be within a provided threshold

        If distance-checking is enabled and no cells are within the specified distance,
        a message is logged to the console with the range of distances encountered among
        cells that meet the angle criterion.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input data from which cells will be selected.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Component Name="targets" NumberOfRequiredValues="1" Extensible="true">
          <BriefDescription>Surface(s) to which the input must be adjacent.</BriefDescription>
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
        </Component>

        <Double Name="angle" Label="Angle threshold" NumberOfRequiredValues="1" Units="degrees">
          <BriefDescription>The maximum allowed angle between opposing surface normals.</BriefDescription>
          <DefaultValue>30.</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Min Inclusive="false">180.</Min>
          </RangeInfo>
        </Double>

        <Double Name="distance" Label="Limit by distance" NumberOfRequiredValues="1"
          Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Only cells within this distance will be selected.</BriefDescription>
          <DetailedDescription>
            Only cells within this distance to a target will be selected, and then only
            if their normal also meets the adjacency criterion.
            The distance is computed from the center of each cell on the source to the
            nearest cell on the surface of any target which satisfies the normal vector
            criterion.
          </DetailedDescription>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(adjacency feature)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
